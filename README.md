# Backup database use Docker
This project provides Docker images to periodically back up database to AWS S3, and to restore from the backup as needed.

Table of Contents

I. [Acknowledgements](#acknowledgements)

- [Build image](#build-image)

II. [Usage](#usage)

1. [MongoDB](#mongodb)

    - [Backup](#backup)

    - [Restore](#restore)

2. [PostgreSQL](#postgresql)

    - [Backup](#backup-1)

    - [Restore](#restore-1)

# Acknowledgements
This project is a reference and re-structuring of [eeshugerman/postgres-backup-s3](https://github.com/eeshugerman/postgres-backup-s3), [schickling/postgres-backup-s3](https://github.com/schickling/dockerfiles/tree/master/postgres-backup-s3) and [schickling/postgres-restore-s3](https://github.com/schickling/dockerfiles/tree/master/postgres-restore-s3).

## Build image

Build Backup image for MongoDB.

```bash
docker build -t pxuanbach/backup:mongo-latest . -f Dockerfile.mongo --build-arg ALPINE_VERSION=3.16 --build-arg TARGETARCH=amd64

# or 
make build-mongo

# 
make build params="mongo"
```

Build Backup image for PostgreSQL.

```bash
docker build -t pxuanbach/backup:postgres-latest . -f Dockerfile.postgres --build-arg ALPINE_VERSION=3.12 --build-arg TARGETARCH=amd64

# or
make build-postgres

# 
make build params="postgres"
```


# Usage

## MongoDB

### Backup
Example:
```yaml
...
services:
  ...
    backup-mongo:
    image: pxuanbach/backup:mongo-latest
    env_file: .env
    environment:
      LOCAL_CLEAN: 'no'          # keep step on local
      S3_CLEAN: 'yes'            # keep step on s3
      PASSPHRASE: ''            # optional
      EXTRA_OPTS: ''            # optional
      EXTRA_OPTS_RESTORE: ''    # optional
      SCHEDULE: '@daily'        # optional
      BACKUP_KEEP_STEPS: 7      # optional
      STEP: 86400               # 1 day = 86400 seconds
```

Environment varibles:
```
MONGODB_HOST=
MONGODB_PORT=27017
MONGODB_USER=
MONGODB_PASS=
MONGODB_DB=
S3_REGION=
S3_ACCESS_KEY_ID=
S3_SECRET_ACCESS_KEY=
S3_BUCKET= 
S3_PREFIX=backup
S3_S3V4='no'
```

- The `SCHEDULE` variable determines backup frequency. See go-cron schedules documentation [here](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules). Omit to run the backup immediately and then exit.
- If `PASSPHRASE` is provided, the backup will be encrypted using GPG.
- Run `docker exec <container name> sh backup.sh` to trigger a backup ad-hoc.
- If `BACKUP_KEEP_STEPS` is set, backups older than this many `STEP` will be deleted from S3.
- Set `STEP`, by default set 86400 (~ 1 day).
- Set `S3_ENDPOINT` if you're using a non-AWS S3-compatible storage provider.
- The `S3_CLEAN` is a flag to scan and delete old backup files on S3.
- The `LOCAL_CLEAN` is a flag to scan and delete old backup files on local.

Use `LOCAL_CLEAN`:
```yml
...
services:
  backup-mongo:
    image: pxuanbach/backup:mongo-latest
    volumes:
      - backup-data:/backup/:delegated
    env_file: .env
    environment:
      LOCAL_CLEAN: 'yes'         # keep step on local
      S3_CLEAN: 'yes'            # keep step on s3
      PASSPHRASE: ''            # optional
      EXTRA_OPTS: ''            # optional
      EXTRA_OPTS_RESTORE: ''    # optional
      SCHEDULE: '@daily'        # optional
      BACKUP_KEEP_STEPS: 7      # optional
      STEP: 86400               # 1 day = 86400 seconds

volumes:
  backup-data:
```

Run Docker Container:
```bash
docker run -d --env-file ./src/mongo/.env \
-e S3_CLEAN="yes" \
-e PASSPHRASE="passphrase" \
-e SCHEDULE="@daily" \
-e BACKUP_KEEP_STEPS=7 \
-e STEP=86400 \
pxuanbach/backup:mongo-latest
```

### Restore
> **WARNING:** Regenerate the data with database name = `MONGODB_DB`.

`S3_CLEAN` and `LOCAL_CLEAN` used to find backup files to restore.
`S3_CLEAN` takes precedence over `LOCAL_CLEAN` if both equal "yes".

From latest backup
```bash
docker compose exec backup-mongo sh restore.sh
# or
docker exec <container_name> sh restore.sh
```

From specific backup
```bash
docker compose exec backup-mongo sh restore.sh <timestamp>
# or
docker exec <container_name> sh restore.sh <timestamp>
```

timestamp format is `%Y-%m-%dT%H.%M.%S`.
Example: `2023-04-04T07.01.00`, `2022-12-30T18.55.36`,...

## PostgreSQL

### Backup

### Restore