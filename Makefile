DOCKER_USERNAME := pxuanbach
IMAGE_NAME := backup
MONGO_IMAGE_TAG := mongo
POSTGRES_IMAGE_TAG := postgres
ALPINE_VERSION := 3.12
params := mongo

build:
	docker build -t $(DOCKER_USERNAME)/$(IMAGE_NAME):$(params)-latest . -f Dockerfile.$(params) --build-arg ALPINE_VERSION=$(ALPINE_VERSION) --build-arg TARGETARCH=amd64

push: 
	docker push $(DOCKER_USERNAME)/$(IMAGE_NAME):$(params)-latest

build-mongo:
	docker build -t $(DOCKER_USERNAME)/$(IMAGE_NAME):$(MONGO_IMAGE_TAG)-latest . -f Dockerfile.$(MONGO_IMAGE_TAG) --build-arg ALPINE_VERSION=$(ALPINE_VERSION) --build-arg TARGETARCH=amd64

push-mongo:
	docker push $(DOCKER_USERNAME)/$(IMAGE_NAME):$(MONGO_IMAGE_TAG)-latest

build-postgres:
	docker build -t $(DOCKER_USERNAME)/$(IMAGE_NAME):$(POSTGRES_IMAGE_TAG)-latest . -f Dockerfile.$(POSTGRES_IMAGE_TAG) --build-arg ALPINE_VERSION=$(ALPINE_VERSION) --build-arg TARGETARCH=amd64

push-postgres:
	docker push $(DOCKER_USERNAME)/$(IMAGE_NAME):$(POSTGRES_IMAGE_TAG)-latest

