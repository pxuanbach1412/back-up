#! /bin/sh

set -u
set -o pipefail

source ./env.sh

S3_URI_BASE="s3://${S3_BUCKET}/${S3_PREFIX}"
POSTGRES_HOST_STR=""
if [[ -n "$POSTGRES_HOST" ]]; then 
  POSTGRES_HOST_STR=" -h ${POSTGRES_HOST}"
fi
POSTGRES_PORT_STR=""
if [[ -n "$POSTGRES_PORT" ]]; then 
  POSTGRES_PORT_STR=" -p ${POSTGRES_PORT}"
fi
USER_STR=""
if [[ -n "$POSTGRES_USER" ]]; then
  USER_STR=" -U ${POSTGRES_USER}"
fi
DB_STR=""
if [[ -n "$POSTGRES_DB" ]]; then 
  DB_STR=" -d ${POSTGRES_DB}"
fi
if [ -z "$PASSPHRASE" ]; then
  file_type=".gz"
else
  file_type=".gz.gpg"
fi

if [ $# -eq 1 ]; then
  timestamp="$1"
  key_suffix="${POSTGRES_DB}_${timestamp}${file_type}"
  if [ "$S3_CLEAN" == "yes" ]; then
    echo "Fetching backup from S3..."
    aws $aws_args s3 cp "${S3_URI_BASE}/${key_suffix}" "db${file_type}"
    key_suffix="db${file_type}"
  else
    echo "Finding latest backup from local..."
    key_suffix=backup/${key_suffix}
  fi
else
  if [ "$S3_CLEAN" == "yes" ]; then
    echo "Finding latest backup from S3..."
    key_suffix=$(
      aws $aws_args s3 ls "${S3_URI_BASE}/${POSTGRES_DB}" \
        | sort \
        | tail -n 1 \
        | awk '{ print $4 }'
    )
    echo "Fetching backup from S3..."
    aws $aws_args s3 cp "${S3_URI_BASE}/${key_suffix}" "db${file_type}"
    key_suffix="db${file_type}"
  else
    echo "Finding latest backup from local..."
    key_suffix=backup/$(
      ls ./backup -Art \
      | tail -n 1 
    )
  fi
fi

echo "Backup file is ${key_suffix}"

if [ -n "$PASSPHRASE" ]; then
  echo "Decrypting backup..."
  gpg --decrypt --batch --passphrase "$PASSPHRASE" "$key_suffix" > db.gz
  rm "$key_suffix"
  key_suffix="db.gz"
fi

echo "Restoring from backup..."
pg_restore $POSTGRES_HOST_STR$POSTGRES_PORT_STR $USER_STR$DB_STR $EXTRA_OPTS_RESTORE --clean --if-exists $key_suffix

# rm "$key_suffix"

echo "Restore complete."