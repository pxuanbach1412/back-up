if [ -z "$POSTGRES_HOST" ]; then
  echo "You need to set the POSTGRES_HOST environment variable."
  exit 1
fi

if [ -z "$POSTGRES_PORT" ]; then
  echo "You need to set the POSTGRES_PORT environment variable."
  exit 1
fi

if [[ -z "$POSTGRES_USER" ]]; then
  echo "POSTGRES_USER environment variable not set, use default value 'postgres'."
  POSTGRES_USER="postgres"
fi

if [[ -z "$POSTGRES_PASSWORD" ]]; then
  echo "You need to set the POSTGRES_PASSWORD environment variable."
  exit 1
fi

if [ -z "$POSTGRES_DB" ]; then
  echo "You need to set the POSTGRES_DB environment variable."
  exit 1
fi

if [ "$S3_CLEAN" == "yes" ]; then
  if [ -z "$S3_BUCKET" ]; then
    echo "You need to set the S3_BUCKET environment variable."
    exit 1
  fi
fi

if [ -z "$S3_ENDPOINT" ]; then
  aws_args=""
else
  aws_args="--endpoint-url $S3_ENDPOINT"
fi

if [ -n "$S3_ACCESS_KEY_ID" ]; then
  export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
fi
if [ -n "$S3_SECRET_ACCESS_KEY" ]; then
  export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
fi
export AWS_DEFAULT_REGION=$S3_REGION

if [ "$S3_CLEAN" == "yes" ]; then
  echo "Enable S3_CLEAN"
fi


if [ -n "$EXTRA_OPTS" ]; then
  echo "EXTRA_OPTS environment variable set $EXTRA_OPTS"
fi

if [ -n "$EXTRA_OPTS_RESTORE" ]; then
  echo "EXTRA_OPTS_RESTORE environment variable set $EXTRA_OPTS_RESTORE"
fi

if [ "$LOCAL_CLEAN" == "yes" ]; then
  echo "Enable LOCAL_CLEAN"
fi

if [ -z "$SCHEDULE" ]; then
  echo "SCHEDULE environment variable not set, run 1 time."
else
  if [ -z "$STEP" ]; then
    echo "STEP environment variable not set, use default value '86400'."
    STEP="86400"
  else
    echo "STEP environment variable set $STEP"
  fi

  if [ -z "$BACKUP_KEEP_STEPS" ]; then
    echo "BACKUP_KEEP_STEPS environment variable not set, use default value '7'."
    BACKUP_KEEP_STEPS="7"
  else
    echo "BACKUP_KEEP_STEPS environment variable set $BACKUP_KEEP_STEPS"
  fi
fi

export PGPASSWORD=$POSTGRES_PASSWORD