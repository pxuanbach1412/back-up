#! /bin/sh

set -eu
set -o pipefail

source ./env.sh

# change the current directory to usr/bin folder of the container
cd /usr/bin 
echo "💿 Backup started at $(date)"

TIMESTAMP=$(date +"%Y-%m-%dT%H.%M.%S")
FILE_NAME="$POSTGRES_DB"_"$TIMESTAMP"
S3_URI_BASE="s3://${S3_BUCKET}/${S3_PREFIX}/${FILE_NAME}.gz"
LOCAL_FILE="/backup/${FILE_NAME}.gz"
USER_STR=""
if [[ -n "$POSTGRES_USER" ]]; then
  USER_STR=" -U ${POSTGRES_USER}"
fi
DB_STR=""
if [[ -n "$POSTGRES_DB" ]]; then 
  DB_STR=" -d ${POSTGRES_DB}"
fi

# if pg_dump command is successful echo success message else echo failure message
if 
  pg_dump -h "$POSTGRES_HOST" -p "$POSTGRES_PORT" $USER_STR$DB_STR --format=custom --compress=6 $EXTRA_OPTS > ../../backup/${FILE_NAME}.gz && cd ../../ 
then
  if [ -n "$PASSPHRASE" ]; then
    echo "Encrypting backup..."
    gpg --symmetric --batch --passphrase "$PASSPHRASE" /backup/${FILE_NAME}.gz
    rm /backup/${FILE_NAME}.gz
    LOCAL_FILE="/backup/${FILE_NAME}.gz.gpg"
    S3_URI="${S3_URI_BASE}.gpg"
  else
    LOCAL_FILE="/backup/${FILE_NAME}.gz"
    S3_URI="${S3_URI_BASE}"
  fi
  echo "💿 😊 👍 Backup completed successfully at $TIMESTAMP" 
else
  echo  "📛❌📛❌ Backup failed at $(date)" 
fi

if [ -z "$S3_BUCKET" ]; then
  echo "Uploading backup to $S3_BUCKET..."
  aws $aws_args s3 cp "$LOCAL_FILE" "$S3_URI"
fi

if [ "$LOCAL_CLEAN" != "yes" ]; then
  echo "Cleaning up... 🧹"
  # Clean up by removing the backup folder
  rm -rf /backup/*
fi

echo "Backup complete 🎉"

if [ -n "$BACKUP_KEEP_STEPS" ]; then
  SEC=$((STEP*BACKUP_KEEP_STEPS))

  if [ "$LOCAL_CLEAN" == "yes" ]; then
    echo "Removing old backups from local..."
    DATE_FROM_REMOVE=$(date -d "@$(($(date +%s) - SEC))" +"%Y-%m-%d %H:%M:%S")
    touch --date "$DATE_FROM_REMOVE" /time
    find /backup -type f ! -newer /time | xargs rm -rf
    rm /time
    echo "Complete local remove."
  fi

  if [ "$S3_CLEAN" == "yes" ]; then
    echo "Removing old backups from $S3_BUCKET..."
    DATE_FROM_REMOVE=$(date -d "@$(($(date +%s) - SEC))" +"%Y-%m-%dT%H:%M:%S")
    backups_query="Contents[?LastModified<='${DATE_FROM_REMOVE}'].{Key: Key}"
    aws $aws_args s3api list-objects \
      --bucket "${S3_BUCKET}" \
      --prefix "${S3_PREFIX}" \
      --query "${backups_query}" \
      --output text \
      | xargs -n1 -t -I 'KEY' aws $aws_args s3 rm s3://"${S3_BUCKET}"/'KEY'
    echo "Complete S3 remove."
  fi

  echo "Remove complete!"
fi